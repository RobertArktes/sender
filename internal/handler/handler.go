package handler

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi/v5"
	httpSwagger "github.com/swaggo/http-swagger"
	_ "gitlab.com/RobertArktes/sender/docs"
	"gitlab.com/RobertArktes/sender/internal/models"
	"gitlab.com/RobertArktes/sender/internal/sender"
	"go.uber.org/zap"
)

type Handler struct{}

func (h *Handler) InitRoutes() *chi.Mux {
	router := chi.NewRouter()

	router.Route("/sender", func(r chi.Router) {
		r.Post("/send", h.getTemplate)

		r.Get("/swagger/*", httpSwagger.Handler(
			httpSwagger.URL("http://localhost:8083/sender/swagger/doc.json"),
		))
	})

	return router
}

//@Summary Get mail
//@Tags mail
//@Description mail template receiver
//@ID get-template
//@Param mail body models.FormedMail true "Mail"
//@Accept json
//@Produce json
//@Router /send [post]
func (h *Handler) getTemplate(w http.ResponseWriter, r *http.Request) {
	var NewMail models.FormedMail
	if err := json.NewDecoder(r.Body).Decode(&NewMail); err != nil {
		zap.L().Error("Failed to decode mail", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed get form"))

		return
	}

	sender.SendMail(w, r, NewMail)
	w.WriteHeader(http.StatusOK)

}
