package sender

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/spf13/viper"
	"gitlab.com/RobertArktes/sender/internal/models"
	"go.uber.org/zap"
)

func SendMail(w http.ResponseWriter, r *http.Request, mail models.FormedMail) {
	log.Println(mail.Text)

	var successStatus bool
	var statusError string
	var err error

	if successStatus = New().Bool(); !successStatus {
		statusError = time.Now().String()
	}

	newRecord := models.MailHistoryRecord{
		UserID:      mail.UserID,
		MailID:      mail.MailID,
		Status:      successStatus,
		StatusError: statusError,
	}
	var mailRecord []byte

	if mailRecord, err = json.Marshal(newRecord); err != nil {
		zap.L().Error("Failed to marshal mail", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to send mail"))

		return
	}

	req, err := http.NewRequestWithContext(r.Context(), "POST", viper.GetString("profile.address"), bytes.NewBuffer(mailRecord))
	if err != nil {
		zap.L().Error("Failed to create request", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to send mail"))

		return
	}

	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		zap.L().Error("Failed to do the request", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to send mail"))

		return
	}

	if err = resp.Body.Close(); err != nil {
		zap.L().Error("Failed to close body", zap.Error(err))
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to send mail"))

		return
	}
}
