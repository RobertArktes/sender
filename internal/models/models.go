package models

type Receiver struct {
	UserID     string `json:"user_id"`     // the owner
	ReceiverID string `json:"receiver_id"` // receiver from owner
}

type ReceiverRequest struct {
	*Receiver
}

type MailTemplateMetadata struct {
	FileName   string   `json:"file_name"`
	ID         string   `json:"id,omitempty"`
	Parameters []string `json:"parameters"`
}

type MailTemplate struct {
	Metadata     MailTemplateMetadata `json:"metadata"`
	TemplateText *string              `json:"template_text"`
}

type Mail struct {
	UserID      string            `json:"user_id"`      // the owner
	ReceiversID []string          `json:"receivers_id"` // receivers from owner
	MailID      string            `json:"mail_id"`
	Template    *MailTemplate     `json:"template"`
	Parameters  map[string]string `json:"parameters"` // receivers from owner
}

type FormedMail struct {
	UserID      string   `json:"user_id"`      // the owner
	ReceiversID []string `json:"receivers_id"` // receivers from owner
	MailID      string   `json:"mail_id"`
	Text        string   `json:"text"`
}

type MailHistoryRecord struct {
	UserID      string `json:"user_id"` // the owner
	MailID      string `json:"mail_id"`
	Status      bool   `json:"status"`       // status of sent mail
	StatusError string `json:"status_error"` // error text if not mail was unsuccessful
}
